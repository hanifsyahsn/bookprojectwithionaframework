package com.example.BookProjectHanif.interfaces;

import java.math.BigDecimal;

public interface Interface {
 BigDecimal discountRate =  new BigDecimal(0.1);
 BigDecimal taxRate = new BigDecimal(0.05);
 BigDecimal costRate = new BigDecimal(1.5);
}
