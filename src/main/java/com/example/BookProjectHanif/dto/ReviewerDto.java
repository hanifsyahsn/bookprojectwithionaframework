package com.example.BookProjectHanif.dto;

public class ReviewerDto {
	private Long reviewId;
	private String reviewName;
	private String country;
	private Boolean verified;
	public ReviewerDto() {
		super();
	}
	public ReviewerDto(Long reviewId, String reviewName, String country, Boolean verified) {
		super();
		this.reviewId = reviewId;
		this.reviewName = reviewName;
		this.country = country;
		this.verified = verified;
	}
	public Long getReviewId() {
		return reviewId;
	}
	public void setReviewId(Long reviewId) {
		this.reviewId = reviewId;
	}
	public String getReviewName() {
		return reviewName;
	}
	public void setReviewName(String reviewName) {
		this.reviewName = reviewName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Boolean getVerified() {
		return verified;
	}
	public void setVerified(Boolean verified) {
		this.verified = verified;
	}

}
