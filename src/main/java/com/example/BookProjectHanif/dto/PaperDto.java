package com.example.BookProjectHanif.dto;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class PaperDto {
	private Long paperId;
	private String qualityName;
	private BigDecimal paperPrice;
	public PaperDto() {
		super();
	}
	public PaperDto(Long paperId, String qualityName, BigDecimal paperPrice) {
		super();
		this.paperId = paperId;
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
	}
}
