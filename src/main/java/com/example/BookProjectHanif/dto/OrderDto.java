package com.example.BookProjectHanif.dto;

import java.math.BigDecimal;
import java.util.Date;

public class OrderDto {
	private Long orderId;
	private CustomersDto customers;
	private Date orderDate;
	private BigDecimal totalOrder;
	public OrderDto() {
		super();
	}
	public OrderDto(Long orderId, CustomersDto customers, Date orderDate, BigDecimal totalOrder) {
		super();
		this.orderId = orderId;
		this.customers = customers;
		this.orderDate = orderDate;
		this.totalOrder = totalOrder;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public CustomersDto getCustomers() {
		return customers;
	}
	public void setCustomers(CustomersDto customers) {
		this.customers = customers;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public BigDecimal getTotalOrder() {
		return totalOrder;
	}
	public void setTotalOrder(BigDecimal totalOrder) {
		this.totalOrder = totalOrder;
	}


}
