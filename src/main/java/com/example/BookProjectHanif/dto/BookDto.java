package com.example.BookProjectHanif.dto;

import java.math.BigDecimal;
import java.util.Date;



public class BookDto {
	private Long bookId;
	private String title;
	private Date releaseDate;
	private AuthorDto authors;
	private PublisherDto publishers;
	private BigDecimal price;
	public BookDto() {
		super();
	}
	public BookDto(Long bookId, String title, Date releaseDate, AuthorDto authors, PublisherDto publishers,
			BigDecimal price) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.releaseDate = releaseDate;
		this.authors = authors;
		this.publishers = publishers;
		this.price = price;
	}
	public Long getBookId() {
		return bookId;
	}
	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public AuthorDto getAuthors() {
		return authors;
	}
	public void setAuthors(AuthorDto authors) {
		this.authors = authors;
	}
	public PublisherDto getPublishers() {
		return publishers;
	}
	public void setPublishers(PublisherDto publishers) {
		this.publishers = publishers;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
