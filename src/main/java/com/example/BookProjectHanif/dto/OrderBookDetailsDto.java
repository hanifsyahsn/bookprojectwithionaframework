package com.example.BookProjectHanif.dto;

import java.math.BigDecimal;

import com.example.BookProjectHanif.models.OrderBookDetailsKey;

public class OrderBookDetailsDto {
	private OrderBookDetailsKey orderKey;
	private int quantity;
	private BigDecimal discount;
	private BigDecimal tax;
	private BookDto book;
	private OrderDto order;
	public OrderBookDetailsDto() {
		super();
	}
	public OrderBookDetailsDto(OrderBookDetailsKey orderKey, int quantity, BigDecimal discount, BigDecimal tax,
			BookDto book, OrderDto order) {
		super();
		this.orderKey = orderKey;
		this.quantity = quantity;
		this.discount = discount;
		this.tax = tax;
		this.book = book;
		this.order = order;
	}
	
	public BookDto getBook() {
		return book;
	}
	public void setBook(BookDto book) {
		this.book = book;
	}
	public OrderDto getOrder() {
		return order;
	}
	public void setOrder(OrderDto order) {
		this.order = order;
	}
	public OrderBookDetailsKey getOrderKey() {
		return orderKey;
	}
	public void setOrderKey(OrderBookDetailsKey orderKey) {
		this.orderKey = orderKey;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public BigDecimal getTax() {
		return tax;
	}
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}
	

}
