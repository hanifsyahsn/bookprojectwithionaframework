package com.example.BookProjectHanif.api.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.OrderDto;
import com.example.BookProjectHanif.models.Order;
import com.io.iona.springboot.controllers.HibernateCRUDController;
@RestController
@RequestMapping("/api/new/order")
public class NewOrderController extends HibernateCRUDController<Order, OrderDto>{

}
