package com.example.BookProjectHanif.api.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.RatingDto;
import com.example.BookProjectHanif.models.Rating;
import com.io.iona.springboot.controllers.HibernateCRUDController;
@RestController
@RequestMapping("/api/new/rating")
public class NewRatingController extends HibernateCRUDController<Rating, RatingDto>{

}
