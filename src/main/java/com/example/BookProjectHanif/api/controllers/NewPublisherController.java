package com.example.BookProjectHanif.api.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.PublisherDto;
import com.example.BookProjectHanif.models.Publisher;
import com.io.iona.springboot.controllers.HibernateCRUDController;
@RestController
@RequestMapping("/api/new/publisher")
public class NewPublisherController extends HibernateCRUDController<Publisher, PublisherDto>{

}
