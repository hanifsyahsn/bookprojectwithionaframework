package com.example.BookProjectHanif.api.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.ReviewerDto;
import com.example.BookProjectHanif.models.Reviewer;
import com.io.iona.springboot.controllers.HibernateCRUDController;
@RestController
@RequestMapping("/api/new/reviewer")
public class NewReviewerController extends HibernateCRUDController<Reviewer, ReviewerDto>{

}
