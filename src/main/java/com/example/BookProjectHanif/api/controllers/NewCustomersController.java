package com.example.BookProjectHanif.api.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.CustomersDto;
import com.example.BookProjectHanif.models.Customers;
import com.io.iona.springboot.controllers.HibernateCRUDController;
@RestController
@RequestMapping("/api/new/customers")
public class NewCustomersController extends HibernateCRUDController<Customers, CustomersDto>{

}
