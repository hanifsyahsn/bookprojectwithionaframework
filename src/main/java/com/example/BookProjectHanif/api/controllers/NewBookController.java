package com.example.BookProjectHanif.api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.BookDto;
import com.example.BookProjectHanif.exceptions.ResourceNotFoundException;
import com.example.BookProjectHanif.models.Author;
import com.example.BookProjectHanif.models.Book;
import com.example.BookProjectHanif.repositories.AuthorRepository;
import com.example.BookProjectHanif.repositories.BookRepository;
import com.example.BookProjectHanif.repositories.PublisherRepository;
import com.io.iona.core.enums.ActionFlow;
import com.io.iona.implementations.pagination.DefaultPagingParameter;
import com.io.iona.springboot.actionflows.custom.CustomAfterReadAll;
import com.io.iona.springboot.actionflows.custom.CustomBeforeInsert;
import com.io.iona.springboot.actionflows.custom.CustomOnInsert;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;
@RestController
@RequestMapping("/api/new/book")
public class NewBookController extends HibernateCRUDController<Book, BookDto> implements CustomOnInsert<Book, BookDto>, CustomAfterReadAll<Book, BookDto>, CustomBeforeInsert<Book, BookDto>{

	@Autowired
	BookRepository bookRepo;
	
	@Autowired
	AuthorRepository authorRepo;
	
	@Autowired
	PublisherRepository publisherRepo;
	
	ModelMapper mapper = new ModelMapper();
	@Override
	public Book onInsert(HibernateDataUtility hibernateDataUtility, HibernateDataSource<Book, BookDto> dataSource) throws Exception {
        Book book = dataSource.getDataModel();
        authorRepo.findById(book.getAuthors().getAuthorId());
        if(authorRepo.findById(book.getAuthors().getAuthorId()).isPresent() == false) {
        	Author authorEntity = new Author();
        	authorEntity.setAge(20);
        	authorEntity.setCountry("Indonesia");
        	authorEntity.setFirstName("Dudung");
        	authorEntity.setLastName("Maman");
        	authorEntity.setGender("male");
        	authorEntity.setRating("Average");
        	System.out.println(authorEntity.getAuthorId());
        	authorRepo.save(authorEntity);
        }
        bookRepo.save(book);
		return dataSource.getDataModel();
	}
	@Override
	public List<BookDto> afterReadAll(HibernateDataUtility dataUtility, HibernateDataSource<Book, BookDto> dataSource,
			DefaultPagingParameter pagingParameter) throws Exception {
		List<Book> listBook = dataSource.getResult(ActionFlow.ON_READ_ALL_ITEMS, List.class);
		List<BookDto> listBookDto = new ArrayList<BookDto>();
		for (Book bookEntity : listBook) {
			BookDto bookEntityDto = new BookDto();
			bookEntityDto = mapper.map(bookEntity, BookDto.class);
			bookEntityDto.setTitle("x" + bookEntity.getTitle());
			listBookDto.add(bookEntityDto);
		}
		return listBookDto;
	}
	@Override
	public void beforeInsert(HibernateDataUtility hibernateDataUtility, HibernateDataSource<Book, BookDto> dataSource) throws Exception {
		Book bookEntity = dataSource.getDataModel();
		BookDto bookEntityDto = mapper.map(bookEntity, BookDto.class);
		publisherRepo.findById(bookEntityDto.getPublishers().getPublisherId()).orElseThrow(() -> new ResourceNotFoundException("Publisher", "Id", bookEntityDto.getPublishers().getPublisherId()));
	}

}
