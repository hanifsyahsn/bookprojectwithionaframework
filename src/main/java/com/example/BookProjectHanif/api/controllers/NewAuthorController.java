package com.example.BookProjectHanif.api.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.AuthorDto;
import com.example.BookProjectHanif.models.Author;
import com.io.iona.springboot.controllers.HibernateCRUDController;
@RestController
@RequestMapping("/api/new/author")
public class NewAuthorController extends HibernateCRUDController<Author, AuthorDto>{

}
