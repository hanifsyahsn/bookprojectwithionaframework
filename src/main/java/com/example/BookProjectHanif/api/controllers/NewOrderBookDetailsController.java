package com.example.BookProjectHanif.api.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.OrderBookDetailsDto;
import com.example.BookProjectHanif.models.OrderBookDetails;
import com.io.iona.springboot.controllers.HibernateCRUDController;
@RestController
@RequestMapping("/api/new/orderbookdetails")
public class NewOrderBookDetailsController extends HibernateCRUDController<OrderBookDetails, OrderBookDetailsDto>{

}
