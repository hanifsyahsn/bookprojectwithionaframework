package com.example.BookProjectHanif.api.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.PaperDto;
import com.example.BookProjectHanif.models.Paper;
import com.io.iona.core.enums.ActionFlow;
import com.io.iona.implementations.pagination.DefaultPagingParameter;
import com.io.iona.springboot.actionflows.custom.CustomAfterReadAll;
import com.io.iona.springboot.actionflows.custom.CustomBeforeReadAll;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;

@RestController
@RequestMapping("/api/new/paper")
public class NewPaperController extends HibernateCRUDController<Paper, PaperDto> implements CustomAfterReadAll<Paper, PaperDto>{

//	@Override
//	public void beforeReadAll(HibernateDataUtility dataUtility, HibernateDataSource<Paper, PaperDto> dataSource,
//			DefaultPagingParameter pagingParameter) throws Exception {
//			List<Object> listData = new ArrayList<Object>();
//			listData.add(1);
//			listData.add(2);
//			listData.add(3);
//			pagingParameter.appendFilterWithInClause("paperId", listData);
//	}
//
	@Override
	public List<PaperDto> afterReadAll(HibernateDataUtility dataUtility, HibernateDataSource<Paper, PaperDto> dataSource,
			DefaultPagingParameter pagingParameter) throws Exception {
		List<Paper> listPaper = dataSource.getResult(ActionFlow.ON_READ_ALL_ITEMS, List.class);
		List<PaperDto> listPaperDto = new ArrayList<PaperDto>();
		
		for (Paper paperEntity : listPaper) {
			PaperDto paperEntityDto = new PaperDto();
			paperEntityDto.setPaperId(paperEntity.getPaperId());
			paperEntityDto.setPaperPrice(paperEntity.getPaperPrice());
			paperEntityDto.setQualityName(paperEntity.getQualityName());
			listPaperDto.add(paperEntityDto);
		}
		return listPaperDto;
	}

}
