package com.example.BookProjectHanif.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.RatingDto;
import com.example.BookProjectHanif.models.Rating;
import com.example.BookProjectHanif.repositories.RatingRepository;

@RestController
@RequestMapping("/api/rating")
public class RatingController {
	
	@Autowired
	RatingRepository ratingRepository;
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> createRatingMapper(@RequestBody RatingDto body){
		Map<String, Object> result = new HashMap<String,Object>();
		Rating ratingEntity = mapper.map(body, Rating.class);
		ratingRepository.save(ratingEntity);
		body.setRatingId(ratingEntity.getRatingId());
		result.put("Message", "Create Success");
		result.put("Data", body);
		return result;
	}	
	@PostMapping("/create2")
	@Transactional
	public Map<String, Object> createRatingMapper2(@RequestBody RatingDto body){
		Map<String, Object> result = new HashMap<String,Object>();
		Rating ratingEntity = mapper.map(body, Rating.class);
		ratingRepository.insertRating(ratingEntity.getBook(), ratingEntity.getReviewer(), ratingEntity.getRatingScore());
		body.setRatingId(ratingEntity.getRatingId());
		result.put("Message", "Create Success");
		result.put("Data", body);
		return result;
	}	
	@GetMapping("/readAll")
	public Map<String, Object> getAllRatingMapper(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Rating> listRating = ratingRepository.findAll();
		List<RatingDto> listRatingDto = new ArrayList<RatingDto>();
		for (Rating ratingEntity : listRating) {
			RatingDto ratingDto = mapper.map(ratingEntity, RatingDto.class);
			listRatingDto.add(ratingDto);
		}
		result.put("Message", "Success");
		result.put("Data", listRatingDto);
		return result;
		
	}
	@GetMapping("/readAll2")
	public Map<String, Object> getAllRatingMapper2(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Rating> listRating = ratingRepository.findAllRating();
		List<RatingDto> listRatingDto = new ArrayList<RatingDto>();
		for (Rating ratingEntity : listRating) {
			RatingDto ratingDto = mapper.map(ratingEntity, RatingDto.class);
			listRatingDto.add(ratingDto);
		}
		result.put("Message", "Success");
		result.put("Data", listRatingDto);
		return result;
		
	}
	@GetMapping("/read/{id}")
	Map<String, Object> readByIdMapper(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String,Object>();
		Rating ratingEntity = ratingRepository.findById(id).get();
		RatingDto ratingDto = mapper.map(ratingEntity, RatingDto.class);
		result.put("massage", "suceess");
		result.put("data", ratingDto);
		return result;
		
	}
	@GetMapping("/read2/{id}")
	Map<String, Object> readByIdMapper2(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String,Object>();
		Rating ratingEntity = ratingRepository.findRatingById(id);
		RatingDto ratingDto = mapper.map(ratingEntity, RatingDto.class);
		result.put("massage", "suceess");
		result.put("data", ratingDto);
		return result;
		
	}
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteRatingMapper(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Rating ratingEntity = ratingRepository.findById(id).get();
		RatingDto ratingDto = mapper.map(ratingEntity, RatingDto.class);
		ratingRepository.delete(ratingEntity);
		result.put("msg", "success");
		result.put("data", ratingDto);
		return result;
		
	}
	@DeleteMapping("/delete2/{id}")
	@Transactional
	public Map<String, Object> deleteRatingMapper2(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Rating ratingEntity = ratingRepository.findRatingById(id);
		RatingDto ratingDto = mapper.map(ratingEntity, RatingDto.class);
		ratingRepository.deleteRatingById2(id);
		result.put("msg", "success");
		result.put("data", ratingDto);
		return result;
		
	}
	@PutMapping("/update/{id}")
	public Map<String, Object> updateRatingMapper(@PathVariable(value = "id")Long id, @RequestBody RatingDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Rating ratingEntity = ratingRepository.findById(id).get();
		ratingEntity = mapper.map(body, Rating.class);
		ratingEntity.setRatingId(id);
		ratingRepository.save(ratingEntity);
		body.setRatingId(ratingEntity.getRatingId());
		result.put("msg", "success");
		result.put("data", body);
		return result;

	}
	@PutMapping("/update2/{id}")
	@Transactional
	public Map<String, Object> updateRatingMapper2(@PathVariable(value = "id")Long id, @RequestBody RatingDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Rating ratingEntity = ratingRepository.findRatingById(id);
		ratingEntity = mapper.map(body, Rating.class);
		ratingEntity.setRatingId(id);
		ratingRepository.updateRating(ratingEntity.getBook(), ratingEntity.getReviewer(), ratingEntity.getRatingScore(), ratingEntity.getRatingId());
		body.setRatingId(ratingEntity.getRatingId());
		result.put("msg", "success");
		result.put("data", body);
		return result;

	}


}
