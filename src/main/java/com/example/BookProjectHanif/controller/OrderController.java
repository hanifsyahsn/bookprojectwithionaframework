package com.example.BookProjectHanif.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.OrderDto;
import com.example.BookProjectHanif.models.Order;
import com.example.BookProjectHanif.models.OrderBookDetails;
import com.example.BookProjectHanif.repositories.OrderBookDetailsRepository;
import com.example.BookProjectHanif.repositories.OrderRepository;

@RestController
@RequestMapping("/api/order")
public class OrderController {
	@Autowired
	OrderRepository orderRepository;
	@Autowired
	OrderBookDetailsRepository orderBookDetailsRepository;
	ModelMapper mapper = new ModelMapper();
	@PostMapping("/create")
	public Map<String, Object> createOrderMapper(@RequestBody Order body){
		Map<String, Object> result = new HashMap<String, Object>();
		Order orderEntity = mapper.map(body, Order.class);
		orderRepository.save(orderEntity);
		body.setOrderId(orderEntity.getOrderId());
		result.put("Message", "CreateSuccess");
		result.put("Data", body);
		return result;
		
	}
	@PostMapping("/create2")
	@Transactional
	public Map<String, Object> createOrderMapper2(@RequestBody Order body){
		Map<String, Object> result = new HashMap<String, Object>();
		Order orderEntity = mapper.map(body, Order.class);
		orderRepository.insertOrder(orderEntity.getCustomers(), orderEntity.getOrderDate(), orderEntity.getTotalOrder());
		body.setOrderId(orderEntity.getOrderId());
		result.put("Message", "CreateSuccess");
		result.put("Data", body);
		return result;
		
	}
	public BigDecimal calculateTotalOrder(Order order) {        
        BigDecimal totalOrder = new BigDecimal(0);
        for (OrderBookDetails orderDetailEntity : order.getOrderBookDetails()) {
            totalOrder = totalOrder.add((orderDetailEntity.getBook().getPrice()
            		.multiply(new BigDecimal(orderDetailEntity.getQuantity())))
            		.add(orderDetailEntity.getTax())
            		.subtract(orderDetailEntity.getDiscount()));
        }
        return totalOrder;
    }
	@PostMapping("/updatetotalorder")
	private Map<String, Object> updateAll(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Order> listOrder = orderRepository.findAll();
		
		for (Order orderEntity : listOrder) {
			orderEntity.setTotalOrder(calculateTotalOrder(orderEntity));
			orderRepository.save(orderEntity);
		}
		
		result.put("Message", "Success");
		return result;
	}
	@GetMapping("/readAll")
	public Map<String, Object> getAllOrderMapper(){
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Order> listOrder = orderRepository.findAll();
		List<OrderDto> listOrderDto = new ArrayList<OrderDto>();
		
		for (Order orderEntity : listOrder) {
			OrderDto orderDto = mapper.map(orderEntity, OrderDto.class);
			listOrderDto.add(orderDto);
		}
		result.put("Message", "Read All Success");
		result.put("Data", listOrderDto);
		return result;
		
	}
	@GetMapping("/readAll2")
	public Map<String, Object> getAllOrderMapper2(){
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Order> listOrder = orderRepository.findAllOrder();
		List<OrderDto> listOrderDto = new ArrayList<OrderDto>();
		
		for (Order orderEntity : listOrder) {
			OrderDto orderDto = mapper.map(orderEntity, OrderDto.class);
			listOrderDto.add(orderDto);
		}
		result.put("Message", "Read All Success");
		result.put("Data", listOrderDto);
		return result;
		
	}
	@GetMapping("/read/{id}")
	public Map<String, Object> getOrderByIdMapper(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Order orderEntity = orderRepository.findById(id).get();
		OrderDto orderDto = mapper.map(orderEntity, OrderDto.class);
		result.put("Message", "Read Id Success");
		result.put("Data", orderDto);
		
		return result;
	}
	@GetMapping("/read2/{id}")
	public Map<String, Object> getOrderByIdMapper2(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Order orderEntity = orderRepository.findOrderById(id);
		OrderDto orderDto = mapper.map(orderEntity, OrderDto.class);
		result.put("Message", "Read Id Success");
		result.put("Data", orderDto);
		
		return result;
	}
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteOrderMapper(@PathVariable(value = "id")Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Order orderEntity = orderRepository.findById(id).get();
		OrderDto orderDto = mapper.map(orderEntity, OrderDto.class);
		orderRepository.delete(orderEntity);
		result.put("Message", "Delete Success");
		result.put("Data", orderDto);
		return result;
		
	}
	@DeleteMapping("/delete2/{id}")
	@Transactional
	public Map<String, Object> deleteOrderMapper2(@PathVariable(value = "id")Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Order orderEntity = orderRepository.findOrderById(id);
		OrderDto orderDto = mapper.map(orderEntity, OrderDto.class);
		orderRepository.deleteOrderById2(id);
		result.put("Message", "Delete Success");
		result.put("Data", orderDto);
		return result;
		
	}
	@PutMapping("/update/{id}")
	public Map<String, Object> updateOrderMapper(@PathVariable(value = "id") Long id,@RequestBody OrderDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Order orderEntity = orderRepository.findById(id).get();
		orderEntity = mapper.map(body, Order.class);
		orderEntity.setOrderId(id);
		orderRepository.save(orderEntity);
		body.setOrderId(orderEntity.getOrderId());
		result.put("Message", "Update Success");
		result.put("Data", body);
		return result;
		
	}
	@PutMapping("/update2/{id}")
	@Transactional
	public Map<String, Object> updateOrderMapper2(@PathVariable(value = "id") Long id,@RequestBody OrderDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Order orderEntity = orderRepository.findOrderById(id);
		orderEntity = mapper.map(body, Order.class);
		orderEntity.setOrderId(id);
		orderRepository.updateOrder(orderEntity.getCustomers(), orderEntity.getOrderDate(), orderEntity.getTotalOrder(), orderEntity.getOrderId());
		body.setOrderId(orderEntity.getOrderId());
		result.put("Message", "Update Success");
		result.put("Data", body);
		return result;
		
	}
}
