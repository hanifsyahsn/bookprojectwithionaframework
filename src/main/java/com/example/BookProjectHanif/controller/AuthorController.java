package com.example.BookProjectHanif.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.AuthorDto;
import com.example.BookProjectHanif.models.Author;
import com.example.BookProjectHanif.repositories.AuthorRepository;

@RestController
@RequestMapping("/api/author")
public class AuthorController {

	@Autowired
	AuthorRepository authorRepository;
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> createRatingMapper(@RequestBody AuthorDto body){
		Map<String, Object> result = new HashMap<String,Object>();
		Author authorEntity = mapper.map(body, Author.class);
		authorRepository.save(authorEntity);
		body.setAuthorId(authorEntity.getAuthorId());
		result.put("Message", "Create Success");
		result.put("Data", body);
		return result;
	}
	@PostMapping("/create2")
	@Transactional
	public Map<String, Object> createRatingMapper2(@RequestBody AuthorDto body){
		Map<String, Object> result = new HashMap<String,Object>();
		Author authorEntity = mapper.map(body, Author.class);
		authorRepository.insertAuthor(authorEntity.getFirstName(), authorEntity.getGender(), authorEntity.getLastName(), authorEntity.getAge(), authorEntity.getCountry(), authorEntity.getRating());
		body.setAuthorId(authorEntity.getAuthorId());
		result.put("Message", "Create Success");
		result.put("Data", body);
		return result;
	}
	@GetMapping("/readAll")
	public Map<String, Object> getAllAuthorMapper(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Author> listAuthor = authorRepository.findAll();
		List<AuthorDto> listAuthorDto = new ArrayList<AuthorDto>();
		for (Author authorEntity : listAuthor) {
			AuthorDto authorDto = mapper.map(authorEntity, AuthorDto.class);
			listAuthorDto.add(authorDto);
		}
		result.put("Message", "Success");
		result.put("Data", listAuthorDto);
		return result;
		
	}
	@GetMapping("/readAll2")
	public Map<String, Object> getAllAuthorMapper2(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Author> listAuthor = authorRepository.findAllAuthor();
		List<AuthorDto> listAuthorDto = new ArrayList<AuthorDto>();
		for (Author authorEntity : listAuthor) {
			AuthorDto authorDto = mapper.map(authorEntity, AuthorDto.class);
			listAuthorDto.add(authorDto);
		}
		result.put("Message", "Success");
		result.put("Data", listAuthorDto);
		return result;
		
	}
	@GetMapping("/read/{id}")
	Map<String, Object> readByIdMapper(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String,Object>();
		Author authorEntity = authorRepository.findById(id).get();
		AuthorDto authorDto = mapper.map(authorEntity, AuthorDto.class);
		result.put("massage", "suceess");
		result.put("data", authorDto);
		return result;
		
	}
	@GetMapping("/read2/{id}")
	Map<String, Object> readByIdMapper2(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String,Object>();
		Author authorEntity = authorRepository.findAuthorById(id);
		AuthorDto authorDto = mapper.map(authorEntity, AuthorDto.class);
		result.put("massage", "suceess");
		result.put("data", authorDto);
		return result;
		
	}
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteAuthorMapper(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Author authorEntity = authorRepository.findById(id).get();
		AuthorDto authorDto = mapper.map(authorEntity, AuthorDto.class);
		authorRepository.delete(authorEntity);
		result.put("msg", "success");
		result.put("data", authorDto);
		return result;
		
	}
	@DeleteMapping("/delete2/{id}")
	@Transactional
	public Map<String, Object> deleteAuthorMapper2(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Author authorEntity = authorRepository.findById(id).get();
		AuthorDto authorDto = mapper.map(authorEntity, AuthorDto.class);
		authorRepository.deleteAuthorById2(authorEntity.getAuthorId());
		result.put("msg", "success");
		result.put("data", authorDto);
		return result;
		
	}
	@PutMapping("/update/{id}")
	public Map<String, Object> updateAuthorMapper(@PathVariable(value = "id")Long id, @RequestBody AuthorDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Author authorEntity = authorRepository.findById(id).get();
		authorEntity = mapper.map(body, Author.class);
		authorEntity.setAuthorId(id);
		authorRepository.save(authorEntity);
		body.setAuthorId(authorEntity.getAuthorId());
		result.put("msg", "success");
		result.put("data", body);
		return result;

	}
	@PutMapping("/update2/{id}")
	@Transactional
	public Map<String, Object> updateAuthorMapper2(@PathVariable(value = "id")Long id, @RequestBody AuthorDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Author authorEntity = authorRepository.findById(id).get();
		authorEntity = mapper.map(body, Author.class);
		authorEntity.setAuthorId(id);
		authorRepository.updateAuthor(authorEntity.getFirstName(), authorEntity.getGender(), authorEntity.getLastName(), authorEntity.getAge(), authorEntity.getCountry(), authorEntity.getRating(), id);
		body.setAuthorId(authorEntity.getAuthorId());
		result.put("msg", "success");
		result.put("data", body);
		return result;

	}
}
