package com.example.BookProjectHanif.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.PaperDto;
import com.example.BookProjectHanif.models.Paper;
import com.example.BookProjectHanif.repositories.PaperRepository;



@RestController
@RequestMapping("/api/paper")
public class PaperController {
	
	ModelMapper mapper = new ModelMapper();
	
	@Autowired
	PaperRepository paperRepository;
	
	@PostMapping("/create")
	public Map<String, Object> createPaperMapper(@RequestBody PaperDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Paper paperEntity = mapper.map(body, Paper.class);
		paperRepository.save(paperEntity);
		body.setPaperId(paperEntity.getPaperId());
		result.put("Message", "CreateSuccess");
		result.put("Data", body);
		return result;
		
	}
	@PostMapping("/create2")
	@Transactional
	public Map<String, Object> createPaperMapper2(@RequestBody PaperDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Paper paperEntity = mapper.map(body, Paper.class);
		paperRepository.insertPaper(paperEntity.getQualityName(), paperEntity.getPaperPrice());
		body.setPaperId(paperEntity.getPaperId());
		result.put("Message", "CreateSuccess");
		result.put("Data", body);
		return result;
		
	}
	@GetMapping("/readAll")
	public Map<String, Object> getAllPaperMapper(){
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Paper> listPaper = paperRepository.findAll();
		List<PaperDto> listPaperDto = new ArrayList<PaperDto>();
		
		for (Paper paperEntity : listPaper) {
			PaperDto paperDto = mapper.map(paperEntity, PaperDto.class);
			listPaperDto.add(paperDto);
		}
		result.put("Message", "Read All Success");
		result.put("Data", listPaperDto);
		return result;
		
	}
	@GetMapping("/readAll2")
	public Map<String, Object> getAllPaperMapper2(){
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Paper> listPaper = paperRepository.findAllPaper();
		List<PaperDto> listPaperDto = new ArrayList<PaperDto>();
		
		for (Paper paperEntity : listPaper) {
			PaperDto paperDto = mapper.map(paperEntity, PaperDto.class);
			listPaperDto.add(paperDto);
		}
		result.put("Message", "Read All Success");
		result.put("Data", listPaperDto);
		return result;
		
	}
	@GetMapping("/read/{id}")
	public Map<String, Object> getPaperByIdMapper(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Paper paperEntity = paperRepository.findById(id).get();
		PaperDto paperDto = mapper.map(paperEntity, PaperDto.class);
		result.put("Message", "Read Id Success");
		result.put("Data", paperDto);
		
		return result;
	}
	@GetMapping("/read2/{id}")
	public Map<String, Object> getPaperByIdMapper2(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Paper paperEntity = paperRepository.findPaperById(id);
		PaperDto paperDto = mapper.map(paperEntity, PaperDto.class);
		result.put("Message", "Read Id Success");
		result.put("Data", paperDto);
		
		return result;
	}
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deletePaperMapper(@PathVariable(value = "id")Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Paper paperEntity = paperRepository.findPaperById(id);
		PaperDto paperDto = mapper.map(paperEntity, PaperDto.class);
		paperRepository.delete(paperEntity);
		result.put("Message", "Delete Success");
		result.put("Data", paperDto);
		return result;
		
	}
	@DeleteMapping("/delete2/{id}")
	@Transactional
	public Map<String, Object> deletePaperMapper2(@PathVariable(value = "id")Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		paperRepository.deletePaperById2(id);
		result.put("Message", "Delete Success");
		return result;
	}
	@PutMapping("/update/{id}")
	public Map<String, Object> updatePaperMapper(@PathVariable(value = "id") Long id,@RequestBody PaperDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Paper paperEntity = paperRepository.findPaperById(id);
		paperEntity = mapper.map(body, Paper.class);
		paperEntity.setPaperId(id);
		paperRepository.save(paperEntity);
		body.setPaperId(paperEntity.getPaperId());
		result.put("Message", "Update Success");
		result.put("Data", body);
		return result;
		
	}
	@PutMapping("/update2/{id}")
	@Transactional
	public Map<String, Object> updatePaperMapper2(@PathVariable(value = "id") Long id,@RequestBody PaperDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		paperRepository.updatePaper(body.getQualityName(), body.getPaperPrice(), id);
	
		result.put("Message", "Update Success");
		result.put("Data", body);
		return result;
		
	}
}
