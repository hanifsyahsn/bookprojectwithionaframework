package com.example.BookProjectHanif.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.BookDto;
import com.example.BookProjectHanif.interfaces.Interface;
import com.example.BookProjectHanif.models.Book;
import com.example.BookProjectHanif.models.Publisher;
import com.example.BookProjectHanif.repositories.AuthorRepository;
import com.example.BookProjectHanif.repositories.BookRepository;
import com.example.BookProjectHanif.repositories.PublisherRepository;

@RestController
@RequestMapping("/api/book")
public class BookController implements Interface{
	
	@Autowired
	BookRepository bookRepository;
	@Autowired
	PublisherRepository publisherRepository;
	@Autowired
	AuthorRepository authorRepository;
	
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> createBookMapper(@RequestBody BookDto body){
		Map<String, Object> result = new HashMap<String,Object>();
		
		Publisher publisherEntity = publisherRepository.findById(body.getPublishers().getPublisherId()).get();
		
		BigDecimal bookPrice = calculatePrice(publisherEntity);
		body.setPrice(bookPrice);
		Book bookEntity = mapper.map(body, Book.class);
		bookRepository.save(bookEntity);
		body.setBookId(bookEntity.getBookId());
		body.setPrice(bookPrice);
		result.put("Message", "Create Success");
		result.put("Data", body);
		return result;
	}
	private BigDecimal calculatePrice(Publisher publisherEntity) {
		BigDecimal bookPrice = new BigDecimal(0);
		bookPrice = bookPrice.add(costRate.multiply(publisherEntity.getPaper().getPaperPrice()));
		return bookPrice;
	}
	@PostMapping("/updateallprices")
	public Map<String, Object> updateAllPrices(){
		Map<String, Object> result = new HashMap<String,Object>();
		
		List<Book> listBook = bookRepository.findAll();
		for (Book bookEntity : listBook) {
			bookEntity.setPrice(calculatePrice(bookEntity.getPublishers()));
			bookRepository.save(bookEntity);
		}
		result.put("MSG", "SUCCESS");
		return result;
	}
	@PostMapping("/create2")
	@Transactional
	public Map<String, Object> createBookMapper2(@RequestBody BookDto body){
		Map<String, Object> result = new HashMap<String,Object>();
		
		Publisher publisherEntity = publisherRepository.findPubById(body.getPublishers().getPublisherId());
		
		BigDecimal bookPrice = calculatePrice(publisherEntity);
		body.setPrice(bookPrice);
		Book bookEntity = mapper.map(body, Book.class);
		//bookRepository.save(bookEntity);
		bookRepository.insertBook(bookEntity.getTitle(), bookEntity.getReleaseDate(), bookEntity.getAuthors(), bookEntity.getPublishers(), bookEntity.getPrice());
		body.setBookId(bookEntity.getBookId());
		body.setPrice(bookPrice);
		result.put("Message", "Create Success");
		result.put("Data", body);
		return result;
	}
	@GetMapping("/readAll")
	public Map<String, Object> getAllBookMapper(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Book> listBook = bookRepository.findAll();
		List<BookDto> listBookDto = new ArrayList<BookDto>();
		for (Book bookEntity : listBook) {
			BookDto bookDto = mapper.map(bookEntity, BookDto.class);
			listBookDto.add(bookDto);
		}
		result.put("Message", "Success");
		result.put("Data", listBookDto);
		return result;
		
	}
	@GetMapping("/readAll2")
	public Map<String, Object> getAllBookMapper2(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Book> listBook = bookRepository.findAllBook();
		List<BookDto> listBookDto = new ArrayList<BookDto>();
		for (Book bookEntity : listBook) {
			BookDto bookDto = mapper.map(bookEntity, BookDto.class);
			listBookDto.add(bookDto);
		}
		result.put("Message", "Success");
		result.put("Data", listBookDto);
		return result;
		
	}
	@GetMapping("/read/{id}")
	Map<String, Object> readByIdMapper(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String,Object>();
		Book bookEntity = bookRepository.findById(id).get();
		BookDto bookDto = mapper.map(bookEntity, BookDto.class);
		result.put("massage", "suceess");
		result.put("data", bookDto);
		return result;
		
	}
	@GetMapping("/read2/{id}")
	Map<String, Object> readByIdMapper2(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String,Object>();
		Book bookEntity = bookRepository.findBookById(id);
		BookDto bookDto = mapper.map(bookEntity, BookDto.class);
		result.put("massage", "suceess");
		result.put("data", bookDto);
		return result;
		
	}
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteAuthorMapper(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Book bookEntity = bookRepository.findById(id).get();
		BookDto bookDto = mapper.map(bookEntity, BookDto.class);
		bookRepository.delete(bookEntity);
		result.put("msg", "success");
		result.put("data", bookDto);
		return result;
		
	}
	@DeleteMapping("/delete2/{id}")
	@Transactional
	public Map<String, Object> deleteAuthorMapper2(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Book bookEntity = bookRepository.findBookById(id);
		BookDto bookDto = mapper.map(bookEntity, BookDto.class);
		bookRepository.deleteBook(id);
		result.put("msg", "success");
		result.put("data", bookDto);
		return result;
		
	}
	@PutMapping("/update/{id}")
	public Map<String, Object> updateAuthorMapper(@PathVariable(value = "id")Long id, @RequestBody BookDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Book bookEntity = bookRepository.findById(id).get();
		bookEntity = mapper.map(body, Book.class);
		bookEntity.setBookId(id);
		bookRepository.save(bookEntity);
		body.setBookId(bookEntity.getBookId());
		result.put("msg", "success");
		result.put("data", body);
		return result;

	}
	@PutMapping("/update2/{id}")
	@Transactional
	public Map<String, Object> updateAuthorMapper2(@PathVariable(value = "id")Long id, @RequestBody BookDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Book bookEntity = bookRepository.findById(id).get();
		bookEntity = mapper.map(body, Book.class);
		bookEntity.setBookId(id);
		bookRepository.updateBook(bookEntity.getTitle(), bookEntity.getReleaseDate(), bookEntity.getAuthors(), bookEntity.getPublishers(), bookEntity.getPrice(), id);
		body.setBookId(bookEntity.getBookId());
		result.put("msg", "success");
		result.put("data", body);
		return result;

	}

}
