package com.example.BookProjectHanif.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.ReviewerDto;
import com.example.BookProjectHanif.models.Reviewer;
import com.example.BookProjectHanif.repositories.ReviewerRepository;

@RestController
@RequestMapping("/api/reviewer")
public class ReviewerController {

	@Autowired
	ReviewerRepository reviewerRepository;
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> createReviewMapper(@RequestBody ReviewerDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Reviewer reviewerEntity = mapper.map(body, Reviewer.class);
		reviewerRepository.save(reviewerEntity);
		body.setReviewId(reviewerEntity.getReviewId());
		result.put("Message", "CreateSuccess");
		result.put("Data", body);
		return result;
		
	}
	@PostMapping("/create2")
	@Transactional
	public Map<String, Object> createReviewMapper2(@RequestBody ReviewerDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Reviewer reviewerEntity = mapper.map(body, Reviewer.class);
		reviewerRepository.insertReviewer(reviewerEntity.getReviewName(), reviewerEntity.getCountry(), reviewerEntity.getVerified());
		body.setReviewId(reviewerEntity.getReviewId());
		result.put("Message", "CreateSuccess");
		result.put("Data", body);
		return result;
		
	}
	@GetMapping("/readAll")
	public Map<String, Object> getAllReviewMapper(){
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Reviewer> listReview = reviewerRepository.findAll();
		List<ReviewerDto> listReviewDto = new ArrayList<ReviewerDto>();
		
		for (Reviewer reviewEntity : listReview) {
			ReviewerDto reviewDto = mapper.map(reviewEntity, ReviewerDto.class);
			listReviewDto.add(reviewDto);
		}
		result.put("Message", "Read All Success");
		result.put("Data", listReviewDto);
		return result;
		
	}
	@GetMapping("/readAll2")
	public Map<String, Object> getAllReviewMapper2(){
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Reviewer> listReview = reviewerRepository.findAllReviewer();
		List<ReviewerDto> listReviewDto = new ArrayList<ReviewerDto>();
		
		for (Reviewer reviewEntity : listReview) {
			ReviewerDto reviewDto = mapper.map(reviewEntity, ReviewerDto.class);
			listReviewDto.add(reviewDto);
		}
		result.put("Message", "Read All Success");
		result.put("Data", listReviewDto);
		return result;
		
	}
	@GetMapping("/read/{id}")
	public Map<String, Object> getReviewByIdMapper(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Reviewer reviewEntity = reviewerRepository.findById(id).get();
		ReviewerDto reviewDto = mapper.map(reviewEntity, ReviewerDto.class);
		result.put("Message", "Read Id Success");
		result.put("Data", reviewDto);
		
		return result;
	}
	@GetMapping("/read2/{id}")
	public Map<String, Object> getReviewByIdMapper2(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Reviewer reviewEntity = reviewerRepository.findReviewerById(id);
		ReviewerDto reviewDto = mapper.map(reviewEntity, ReviewerDto.class);
		result.put("Message", "Read Id Success");
		result.put("Data", reviewDto);
		
		return result;
	}
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteReviewMapper(@PathVariable(value = "id")Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Reviewer reviewEntity = reviewerRepository.findById(id).get();
		ReviewerDto reviewDto = mapper.map(reviewEntity, ReviewerDto.class);
		reviewerRepository.delete(reviewEntity);
		result.put("Message", "Delete Success");
		result.put("Data", reviewDto);
		return result;
		
	}
	@DeleteMapping("/delete2/{id}")
	@Transactional
	public Map<String, Object> deleteReviewMapper2(@PathVariable(value = "id")Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Reviewer reviewEntity = reviewerRepository.findReviewerById(id);
		ReviewerDto reviewDto = mapper.map(reviewEntity, ReviewerDto.class);
		reviewerRepository.deleteReviewerById2(id);
		result.put("Message", "Delete Success");
		result.put("Data", reviewDto);
		return result;
		
	}
	@PutMapping("/update/{id}")
	public Map<String, Object> updateReviewMapper(@PathVariable(value = "id") Long id,@RequestBody ReviewerDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Reviewer reviewEntity = reviewerRepository.findById(id).get();
		reviewEntity = mapper.map(body, Reviewer.class);
		reviewEntity.setReviewId(id);
		reviewerRepository.save(reviewEntity);
		body.setReviewId(reviewEntity.getReviewId());
		result.put("Message", "Update Success");
		result.put("Data", body);
		return result;
		
	}
	@PutMapping("/update2/{id}")
	@Transactional
	public Map<String, Object> updateReviewMapper2(@PathVariable(value = "id") Long id,@RequestBody ReviewerDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Reviewer reviewEntity = reviewerRepository.findReviewerById(id);
		reviewEntity = mapper.map(body, Reviewer.class);
		reviewEntity.setReviewId(id);
		reviewerRepository.updateReviewer(reviewEntity.getReviewName(), reviewEntity.getCountry(), reviewEntity.getVerified(), reviewEntity.getReviewId());
		body.setReviewId(reviewEntity.getReviewId());
		result.put("Message", "Update Success");
		result.put("Data", body);
		return result;
		
	}

}
