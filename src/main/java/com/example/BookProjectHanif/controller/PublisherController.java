package com.example.BookProjectHanif.controller;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.PublisherDto;
import com.example.BookProjectHanif.models.Publisher;
import com.example.BookProjectHanif.repositories.PublisherRepository;


@RestController
@RequestMapping("/api/publisher")
public class PublisherController {
	
	@Autowired
	PublisherRepository publisherRepository;
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> createPublisherMapper(@RequestBody PublisherDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Publisher publisherEntity = mapper.map(body, Publisher.class);
		publisherRepository.save(publisherEntity);
		body.setPublisherId(publisherEntity.getPublisherId());
		result.put("Message", "Create Success");
		result.put("Data", body);
		return result;
		
	}
	@PostMapping("/create2")
	@Transactional
	public Map<String, Object> createPublisherMapper2(@RequestBody Publisher body){
		Map<String, Object> result = new HashMap<String, Object>();
		Publisher publisherEntity = mapper.map(body, Publisher.class);
		publisherRepository.insertPub2(body.getCompanyName(), body.getCountry(), body.getPaper());
		body.setPublisherId(publisherEntity.getPublisherId());
		result.put("Message", "Create Success");
		result.put("Data", body);
		return result;
		
	}
	@GetMapping("/readAll")
 	public Map<String, Object> getAllPublisherMapper(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Publisher> listPublisher = publisherRepository.findAll();
		List<PublisherDto> listPublisherDto = new ArrayList<PublisherDto>();
		for (Publisher publisherEntity : listPublisher) {
			PublisherDto publisherDto = mapper.map(publisherEntity, PublisherDto.class);
			
			listPublisherDto.add(publisherDto);
		}
		result.put("Message", "Read All Success");
		result.put("Data", listPublisherDto);
		return result;
		
	}
	@GetMapping("/readAll2")
 	public Map<String, Object> getAllPublisherMapper2(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Publisher> listPublisher = publisherRepository.findAllPub();
		List<PublisherDto> listPublisherDto = new ArrayList<PublisherDto>();
		for (Publisher publisherEntity : listPublisher) {
			PublisherDto publisherDto = mapper.map(publisherEntity, PublisherDto.class);
			
			listPublisherDto.add(publisherDto);
		}
		result.put("Message", "Read All Success");
		result.put("Data", listPublisherDto);
		return result;
		
	}
	@GetMapping("/read/{id}")
	public Map<String, Object> getPublisherByIdMapper(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String,Object>();
		Publisher publisherEntity = publisherRepository.findById(id).get();
		PublisherDto publisherDto = mapper.map(publisherEntity, PublisherDto.class);
		result.put("Message", "Read Id Success");
		result.put("Data", publisherDto);
		return result;
		
	}
	@GetMapping("/read2/{id}")
	public Map<String, Object> getPublisherByIdMapper2(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String,Object>();
		Publisher publisherEntity = publisherRepository.findPubById(id);
		PublisherDto publisherDto = mapper.map(publisherEntity, PublisherDto.class);
		result.put("Message", "Read Id Success");
		result.put("Data", publisherDto);
		return result;
		
	}
	@DeleteMapping("/delete/{id}")
	Map<String, Object> deletePublisherMapper(@PathVariable(value = "id")Long id){
		Map<String, Object> result = new HashMap<String,Object>();
		Publisher publisherEntity = publisherRepository.findPubById(id);
		PublisherDto publisherDto = mapper.map(publisherEntity, PublisherDto.class);
		publisherRepository.delete(publisherEntity);
		result.put("Message", "Delete Success");
		result.put("Data", publisherDto);
		return result;
	}
	
	@DeleteMapping("/delete2/{id}")
	@Transactional
	Map<String, Object> deletePublisherMapper2(@PathVariable(value = "id")Long id){
		Map<String, Object> result = new HashMap<String,Object>();
		publisherRepository.deletePubById(id);
		result.put("Message", "Delete Success");
		return result;
	}

	@PutMapping("/update/{id}")
	public Map<String, Object> updatePublisherMapper(@PathVariable(value = "id") Long id, @RequestBody PublisherDto body){
		Map<String, Object> result = new HashMap<String,Object>();
		Publisher publisherEntity = publisherRepository.findPubById(id);
		publisherEntity = mapper.map(body, Publisher.class);
		publisherEntity.setPublisherId(id);
		publisherRepository.save(publisherEntity);
		result.put("Message", "Update Success");
		result.put("Data", body);
		return result;
	}
	@PutMapping("/update2/{id}")
	@Transactional
	public Map<String, Object> updatePublisherMapper2(@PathVariable(value = "id") Long id, @RequestBody PublisherDto body){
		Map<String, Object> result = new HashMap<String,Object>();
		publisherRepository.updatePub(body.getCompanyName(), body.getCountry(), body.getPaper().getPaperId(), id);
		result.put("Message", "Update Success");
		result.put("Data", body);
		return result;
	}

}
