package com.example.BookProjectHanif.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.OrderBookDetailsDto;
import com.example.BookProjectHanif.interfaces.Interface;
import com.example.BookProjectHanif.models.Book;
import com.example.BookProjectHanif.models.OrderBookDetails;
import com.example.BookProjectHanif.models.OrderBookDetailsKey;
import com.example.BookProjectHanif.repositories.BookRepository;
import com.example.BookProjectHanif.repositories.OrderBookDetailsRepository;
import com.example.BookProjectHanif.repositories.OrderRepository;

@RestController
@RequestMapping("/api/orderbookdetails")
public class OrderBookDetailsController implements Interface {
	
	@Autowired
	OrderBookDetailsRepository orderBookRepository;
	@Autowired
	BookRepository bookRepository;
	@Autowired
	OrderRepository orderRepository;
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> createOrderBook(@RequestBody OrderBookDetails body){		
		Map<String, Object> result = new HashMap<String, Object>();	
		Book bookFinder = bookRepository.findById(body.getOrderKey().getBookId()).get();	
		BigDecimal totalTaxFinal = calculateTax(body, bookFinder);	
		body.setTax(totalTaxFinal);
		OrderBookDetails orderBookEntity = mapper.map(body, OrderBookDetails.class);
		orderBookRepository.save(orderBookEntity);
		body.setOrderKey(orderBookEntity.getOrderKey());
		body.setTax(orderBookEntity.getTax());
		result.put("message", "success");
		result.put("data", body);
		return result;
	}
	private BigDecimal calculateTax(OrderBookDetails body, Book bookFinder) {
		BigDecimal totalTax = new BigDecimal(0);
		BigDecimal totalTaxFinal = new BigDecimal(0);
		totalTax = totalTax.add(taxRate.multiply(bookFinder.getPrice()));
		totalTaxFinal = totalTaxFinal.add(totalTax.multiply(new BigDecimal(body.getQuantity())));
		return totalTaxFinal;
	}
	@PostMapping("/updatealltax")
	private Map<String, Object> updateAlltax(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<OrderBookDetails> listOrder = orderBookRepository.findAll();
		
		for (OrderBookDetails orderBookDetailsEntity : listOrder) {
			orderBookDetailsEntity.setTax(calculateTax(orderBookDetailsEntity, orderBookDetailsEntity.getBook()));
			orderBookRepository.save(orderBookDetailsEntity);
		}
		
		result.put("Message", "Success");
		return result;
	}
	private BigDecimal calculateDiscount(OrderBookDetails body) {
        Set<OrderBookDetails> bookDetailsEntity = body.getOrder().getOrderBookDetails();
        return discountValidator(body, bookDetailsEntity);
    }
    private BigDecimal discountValidator(OrderBookDetails body, Set<OrderBookDetails> bookDetailsEntity) {
        BigDecimal totalDiscount = new BigDecimal(0);
        if(bookDetailsEntity.size()>= 3) {
            totalDiscount = discountCalculator(body, totalDiscount);
            return totalDiscount;
        }
        else {
            return totalDiscount;
        }
    }
    private BigDecimal discountCalculator(OrderBookDetails body, BigDecimal totalDiscount) {
        totalDiscount = totalDiscount.add(discountRate.multiply(body.getBook().getPrice()))
                .multiply(new BigDecimal(body.getQuantity()));
        return totalDiscount;
    }
	@PostMapping("/updatealldiscount")
	private Map<String, Object> updateAllDiscount(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<OrderBookDetails> listOrder = orderBookRepository.findAllData();
		
		for (OrderBookDetails orderBookDetailsEntity : listOrder) {
			orderBookDetailsEntity.setDiscount(calculateDiscount(orderBookDetailsEntity));
			orderBookRepository.save(orderBookDetailsEntity);
		}
		
		result.put("Message", "Success");
		return result;
	}
	@GetMapping("/readAll")
	public Map<String, Object> getAllOrderBookMapper(){
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<OrderBookDetails> listOrder = orderBookRepository.findAll();
		List<OrderBookDetailsDto> listOrderDto = new ArrayList<OrderBookDetailsDto>();
		
		for (OrderBookDetails orderBookEntity : listOrder) {
			OrderBookDetailsDto orderBookDto = mapper.map(orderBookEntity, OrderBookDetailsDto.class);
			listOrderDto.add(orderBookDto);
		}
		result.put("Message", "Read All Success");
		result.put("Data", listOrderDto);
		return result;
		
	}
//	@GetMapping("/readAll2")
//	public Map<String, Object> getAllOrderBookMapper2(){
//		Map<String, Object> result = new HashMap<String, Object>();
//		
//		List<OrderBookDetails> listOrder = orderBookRepository.findAllData();
//		List<OrderBookDetailsDto> listOrderDto = new ArrayList<OrderBookDetailsDto>();
//		
//		for (OrderBookDetails orderBookEntity : listOrder) {
//			OrderBookDetailsDto orderBookDto = mapper.map(orderBookEntity, OrderBookDetailsDto.class);
//			listOrderDto.add(orderBookDto);
//		}
//		result.put("Message", "Read All Success");
//		result.put("Data", listOrderDto);
//		return result;
//		
//	}
	@GetMapping("/read/{idbook}/{idorder}")
	public Map<String, Object> getOrderBookByIdMapper(@PathVariable(value = "idbook") Long bookId, @PathVariable(value = "idorder") Long orderId){
		Map<String, Object> result = new HashMap<String, Object>();
		OrderBookDetailsKey id = new OrderBookDetailsKey(bookId, orderId);
		OrderBookDetails orderBookEntity = orderBookRepository.findById(id).get();
		OrderBookDetailsDto orderBookDto = mapper.map(orderBookEntity, OrderBookDetailsDto.class);
		result.put("Message", "Read Id Success");
		result.put("Data", orderBookDto);
		
		return result;
	}
//	@GetMapping("/read2/{idbook}/{idorder}")
//	public Map<String, Object> getOrderBookByIdMapper2(@PathVariable(value = "idbook") Long bookId, @PathVariable(value = "idorder") Long orderId){
//		Map<String, Object> result = new HashMap<String, Object>();
//		OrderBookDetails orderBookEntity = orderBookRepository.findDataById(bookId, orderId);
//		OrderBookDetailsDto orderBookDto = mapper.map(orderBookEntity, OrderBookDetailsDto.class);
//		result.put("Message", "Read Id Success");
//		result.put("Data", orderBookDto);
//		
//		return result;
//	}
	
	@DeleteMapping("/delete/{idbook}/{idorder}")
	public Map<String, Object> deleteOrderBookMapper(@PathVariable(value = "idbook")Long bookId,@PathVariable(value = "idorder")Long orderId){
		Map<String, Object> result = new HashMap<String, Object>();
		OrderBookDetailsKey id = new OrderBookDetailsKey(bookId, orderId);
		OrderBookDetails orderBookEntity = orderBookRepository.findById(id).get();
		OrderBookDetailsDto orderBookDto = mapper.map(orderBookEntity, OrderBookDetailsDto.class);
		orderBookRepository.delete(orderBookEntity);
		result.put("Message", "Delete Success");
		result.put("Data", orderBookDto);
		return result;
		
	}
//	@DeleteMapping("/delete2/{idbook}/{idorder}")
//	@Transactional
//	public Map<String, Object> deleteOrderBookMapper2(@PathVariable(value = "idbook")Long bookId,@PathVariable(value = "idorder")Long orderId){
//		Map<String, Object> result = new HashMap<String, Object>();
//		orderBookRepository.deleteDataById(bookId, orderId);
//		result.put("Message", "Delete Success");
//		return result;
//		
//	}
	@PutMapping("/update/{idbook}/{idorder}")
	public Map<String, Object> updateOrderBookMapper(@PathVariable(value = "idbook") Long bookId,@PathVariable(value = "idorder") Long orderId,@RequestBody OrderBookDetailsDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		OrderBookDetailsKey id = new OrderBookDetailsKey(bookId, orderId);
		OrderBookDetails orderBookEntity = orderBookRepository.findById(id).get();
		orderBookEntity = mapper.map(body, OrderBookDetails.class);
		orderBookEntity.setOrderKey(body.getOrderKey());
		orderBookRepository.save(orderBookEntity);
		result.put("Message", "Update Success");
		result.put("Data", orderBookEntity);
		return result;
		
	}
//	@PutMapping("/update2/{idbook}/{idorder}")
//	@Transactional
//	public Map<String, Object> updateOrderBookMapper2(@PathVariable(value = "idbook") Long bookId,@PathVariable(value = "idorder") Long orderId,@RequestBody OrderBookDetailsDto body){
//		Map<String, Object> result = new HashMap<String, Object>();
//		orderBookRepository.updateData(body.getQuantity(), body.getDiscount(), body.getTax(), body.getOrderKey().getBookId(), body.getOrderKey().getOrderId(), bookId, orderId);
//		result.put("Message", "Update Success");
//		return result;
//		
//	}
}
