package com.example.BookProjectHanif.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.dto.CustomersDto;
import com.example.BookProjectHanif.models.Customers;
import com.example.BookProjectHanif.repositories.CustomerRepository;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

	@Autowired
	CustomerRepository customerRepository;
	
	ModelMapper mapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> createCustomer(@RequestBody CustomersDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Customers customerEntity = mapper.map(body, Customers.class);
		customerRepository.save(customerEntity);
		body.setCustomerId(customerEntity.getCustomerId());
		result.put("Message", "Success");
		result.put("data", body);
		return result;
	}
	@PostMapping("/create2")
	@Transactional
	public Map<String, Object> createCustomer2(@RequestBody CustomersDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Customers customerEntity = mapper.map(body, Customers.class);
		customerRepository.insertCustomer(customerEntity.getCustomerName(), customerEntity.getAddress(), customerEntity.getPhoneNumber(), customerEntity.getPostalCode(), customerEntity.getEmail(), customerEntity.getCountry());
		body.setCustomerId(customerEntity.getCustomerId());
		result.put("Message", "Success");
		result.put("data", body);
		return result;
	}
	@GetMapping("/readAll")
	public Map<String, Object> getAllCustomerMapper(){
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Customers> listCustomers = customerRepository.findAll();
		List<CustomersDto> listCustomersDto = new ArrayList<CustomersDto>();
		
		for (Customers customersEntity : listCustomers) {
			CustomersDto customersDto = mapper.map(customersEntity, CustomersDto.class);
			listCustomersDto.add(customersDto);
		}
		result.put("Message", "Read All Success");
		result.put("Data", listCustomersDto);
		return result;
		
	}
	@GetMapping("/readAll2")
	public Map<String, Object> getAllCustomerMapper2(){
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<Customers> listCustomers = customerRepository.findAllCustomers();
		List<CustomersDto> listCustomersDto = new ArrayList<CustomersDto>();
		
		for (Customers customersEntity : listCustomers) {
			CustomersDto customersDto = mapper.map(customersEntity, CustomersDto.class);
			listCustomersDto.add(customersDto);
		}
		result.put("Message", "Read All Success");
		result.put("Data", listCustomersDto);
		return result;
		
	}
	@GetMapping("/read/{id}")
	public Map<String, Object> getCustomersByIdMapper(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Customers customersEntity = customerRepository.findById(id).get();
		CustomersDto customersDto = mapper.map(customersEntity, CustomersDto.class);
		result.put("Message", "Read Id Success");
		result.put("Data", customersDto);
		
		return result;
	}
	@GetMapping("/read2/{id}")
	public Map<String, Object> getCustomersByIdMapper2(@PathVariable(value = "id") Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Customers customersEntity = customerRepository.findCustomerById(id);
		CustomersDto customersDto = mapper.map(customersEntity, CustomersDto.class);
		result.put("Message", "Read Id Success");
		result.put("Data", customersDto);
		
		return result;
	}
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteCustomersMapper(@PathVariable(value = "id")Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Customers customersEntity = customerRepository.findById(id).get();
		CustomersDto customersDto = mapper.map(customersEntity, CustomersDto.class);
		customerRepository.delete(customersEntity);
		result.put("Message", "Delete Success");
		result.put("Data", customersDto);
		return result;
	}
	@DeleteMapping("/delete2/{id}")
	@Transactional
	public Map<String, Object> deleteCustomersMapper2(@PathVariable(value = "id")Long id){
		Map<String, Object> result = new HashMap<String, Object>();
		Customers customersEntity = customerRepository.findById(id).get();
		CustomersDto customersDto = mapper.map(customersEntity, CustomersDto.class);
		customerRepository.deleteCustomerById2(id);
		result.put("Message", "Delete Success");
		result.put("Data", customersDto);
		return result;
	}
	@PutMapping("/update/{id}")
	public Map<String, Object> updateCustomerMapper(@PathVariable(value = "id") Long id,@RequestBody CustomersDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Customers customersEntity = customerRepository.findById(id).get();
		customersEntity = mapper.map(body, Customers.class);
		customersEntity.setCustomerId(id);
		customerRepository.save(customersEntity);
		body.setCustomerId(customersEntity.getCustomerId());
		result.put("Message", "Update Success");
		result.put("Data", body);
		return result;
		
	}
	@PutMapping("/update2/{id}")
	@Transactional
	public Map<String, Object> updateCustomerMapper2(@PathVariable(value = "id") Long id,@RequestBody CustomersDto body){
		Map<String, Object> result = new HashMap<String, Object>();
		Customers customersEntity = customerRepository.findById(id).get();
		customersEntity = mapper.map(body, Customers.class);
		customersEntity.setCustomerId(id);
		customerRepository.updateCustomer(customersEntity.getCustomerName(), customersEntity.getCountry(), customersEntity.getAddress(), customersEntity.getPhoneNumber(), customersEntity.getPostalCode(), customersEntity.getEmail(), customersEntity.getCustomerId());
		body.setCustomerId(customersEntity.getCustomerId());
		result.put("Message", "Update Success");
		result.put("Data", body);
		return result;
		
	}

}
