package com.example.BookProjectHanif.ols.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.models.Paper;
import com.example.BookProjectHanif.ols.PaperOLO;
import com.io.iona.springboot.controllers.HibernateOptionListController;
@RestController
@RequestMapping("/ol/paper")
public class PaperOLOController extends HibernateOptionListController<Paper, PaperOLO>{

}
