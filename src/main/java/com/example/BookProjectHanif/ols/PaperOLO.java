package com.example.BookProjectHanif.ols;

import java.math.BigDecimal;

import com.io.iona.core.data.annotations.OptionListKey;

public class PaperOLO {
	@OptionListKey
	private long paperId;
	private String qualityName;
	private BigDecimal paperPrice;
	public long getPaperId() {
		return paperId;
	}
	public void setPaperId(long paperId) {
		this.paperId = paperId;
	}
	public String getQualityName() {
		return qualityName;
	}
	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}
	public BigDecimal getPaperPrice() {
		return paperPrice;
	}
	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}
	

}
