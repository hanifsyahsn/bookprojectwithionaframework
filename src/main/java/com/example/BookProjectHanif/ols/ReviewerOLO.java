package com.example.BookProjectHanif.ols;

import com.io.iona.core.data.annotations.OptionListKey;

public class ReviewerOLO {
	@OptionListKey
	private Long reviewId;
	private String reviewName;
	public Long getReviewId() {
		return reviewId;
	}
	public void setReviewId(Long reviewId) {
		this.reviewId = reviewId;
	}
	public String getReviewName() {
		return reviewName;
	}
	public void setReviewName(String reviewName) {
		this.reviewName = reviewName;
	}

}
