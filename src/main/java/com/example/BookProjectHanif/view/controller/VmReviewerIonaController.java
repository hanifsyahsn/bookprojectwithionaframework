package com.example.BookProjectHanif.view.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.view.dto.VmReviewerIonaDto;
import com.example.BookProjectHanif.views.VmReviewerIona;
import com.io.iona.springboot.controllers.HibernateViewController;
@RestController
@RequestMapping("/api/viewreviewer")
public class VmReviewerIonaController extends HibernateViewController<VmReviewerIona, VmReviewerIonaDto>{

}
