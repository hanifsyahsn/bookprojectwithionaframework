package com.example.BookProjectHanif.view.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.view.dto.VmPaperIonaDto;
import com.example.BookProjectHanif.views.VmPaperIona;
import com.io.iona.springboot.controllers.HibernateViewController;
@RestController
@RequestMapping("/api/viewpaper")
public class VmPaperIonaController extends HibernateViewController<VmPaperIona, VmPaperIonaDto>{

}
