package com.example.BookProjectHanif.view.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectHanif.view.dto.VmBookIonaDto;
import com.example.BookProjectHanif.views.VmBookIona;
import com.io.iona.springboot.controllers.HibernateViewController;
@RestController
@RequestMapping("/api/viewbook")
public class VmBookIonaController extends HibernateViewController<VmBookIona, VmBookIonaDto>{

}
