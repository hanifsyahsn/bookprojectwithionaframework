package com.example.BookProjectHanif.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "paper")
@Getter
@Setter
public class Paper implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5692361932156124093L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_paper_id_paper_seq")
	@SequenceGenerator(name = "generator_paper_id_paper_seq", sequenceName = "paper_id_paper_seq", schema = "public", allocationSize = 1)
	@Column(name = "paper_id", unique = true, nullable = false)
	private Long paperId;
	
	@Column(name = "quality_name", nullable = false)
	private String qualityName;
	
	@Column(name = "price", nullable = false)
	private BigDecimal paperPrice;
	
	@OneToMany(mappedBy = "paper")
	private Set<Publisher> publishers;
	
	public Paper() {
		super();
	}
	public Paper(Long paperId, String qualityName, BigDecimal paperPrice, Set<Publisher> publishers) {
		super();
		this.paperId = paperId;
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
		this.publishers = publishers;
	}
	
	
}
