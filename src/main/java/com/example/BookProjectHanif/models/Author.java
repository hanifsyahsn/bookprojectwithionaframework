package com.example.BookProjectHanif.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "author")
public class Author implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_author_id_author_seq")
	@SequenceGenerator(name = "generator_author_id_author_seq", sequenceName = "author_id_author_seq", schema = "public", allocationSize = 1)
	@Column(name = "author_id", unique = true, nullable = false, insertable = true)
	private Long authorId;
	@Column(nullable = false, name = "first_name")
	private String firstName;
	@Column(nullable = false, name = "gender")
	private String gender;
	@Column(nullable = false, name = "last_name")
	private String lastName;
	@Column(nullable = false, name = "age")
	private int age;
	@Column(nullable = false, name = "contry")
	private String country;
	@Column(nullable = false, name = "rating")
	private String rating;
	@OneToMany(mappedBy = "authors")
	private Set<Book> books;
	public Author() {
		super();
	}
	

	public Author(Long authorId, String firstName, String gender, String lastName, int age, String country,
			String rating, Set<Book> books) {
		super();
		this.authorId = authorId;
		this.firstName = firstName;
		this.gender = gender;
		this.lastName = lastName;
		this.age = age;
		this.country = country;
		this.rating = rating;
		this.books = books;
	}


	public String getRating() {
		return rating;
	}


	public void setRating(String rating) {
		this.rating = rating;
	}


	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Set<Book> getBooks() {
		return books;
	}
	public void setBooks(Set<Book> books) {
		this.books = books;
	}
	public Long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
}
