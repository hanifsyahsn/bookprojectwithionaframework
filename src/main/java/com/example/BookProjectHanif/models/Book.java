package com.example.BookProjectHanif.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "book")
public class Book implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6929955298216695948L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator =  "generator_book_id_book_seq")
	@SequenceGenerator(name = "generator_book_id_book_seq", sequenceName = "book_id_book_seq", schema = "public", allocationSize = 1)
	@Column(name = "book_id", unique = true, nullable = false)
	private Long bookId;
	@Column(nullable = false, name = "title")
	private String title;
	@Column(nullable = false, name = "release_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date releaseDate;
	@ManyToOne
	@JoinColumn(name = "author_id")
	private Author authors;
	@ManyToOne
	@JoinColumn(name = "publisher_id")
	private Publisher publishers;
	@Column(nullable = false, name = "price")
	private BigDecimal price;
	@OneToMany(mappedBy = "book")
	private Set<Rating> ratings;
	@OneToMany(mappedBy = "book")
	private Set<OrderBookDetails> orderBookDetails;
	public Book() {
		super();
	}
	public Book(Long bookId, String title, Date releaseDate, Author authors, Publisher publishers, BigDecimal price,
			Set<Rating> ratings, Set<OrderBookDetails> orderBookDetails) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.releaseDate = releaseDate;
		this.authors = authors;
		this.publishers = publishers;
		this.price = price;
		this.ratings = ratings;
		this.orderBookDetails = orderBookDetails;
	}

	public Set<OrderBookDetails> getOrderBookDetails() {
		return orderBookDetails;
	}
	public void setOrderBookDetails(Set<OrderBookDetails> orderBookDetails) {
		this.orderBookDetails = orderBookDetails;
	}
	public Set<Rating> getRatings() {
		return ratings;
	}
	public void setRatings(Set<Rating> ratings) {
		this.ratings = ratings;
	}

	public Long getBookId() {
		return bookId;
	}
	public Author getAuthors() {
		return authors;
	}
	public void setAuthors(Author authors) {
		this.authors = authors;
	}
	public Publisher getPublishers() {
		return publishers;
	}
	public void setPublishers(Publisher publishers) {
		this.publishers = publishers;
	}
	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	

}
