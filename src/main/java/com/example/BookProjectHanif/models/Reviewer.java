package com.example.BookProjectHanif.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "reviewer")
public class Reviewer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_review_id_review_seq")
	@SequenceGenerator(name = "generator_review_id_review_seq", sequenceName = "review_id_review_seq", schema = "public", allocationSize = 1)
	@Column(name = "review_id", unique = true, nullable = false)
	private Long reviewId;
	@Column(name = "review_name", nullable = false)
	private String reviewName;
	@Column(name = "country", nullable = false)
	private String country;
	@Column(name = "verified", nullable = false)
	private Boolean verified;
	@OneToMany(mappedBy = "reviewer")
	private Set<Rating> ratings;
	public Reviewer() {
		super();
	}
	
	public Reviewer(Long reviewId, String reviewName, String country, Boolean verified, Set<Rating> ratings) {
		super();
		this.reviewId = reviewId;
		this.reviewName = reviewName;
		this.country = country;
		this.verified = verified;
		this.ratings = ratings;
	}

	public Set<Rating> getRatings() {
		return ratings;
	}
	public void setRatings(Set<Rating> ratings) {
		this.ratings = ratings;
	}

	public Long getReviewId() {
		return reviewId;
	}
	public void setReviewId(Long reviewId) {
		this.reviewId = reviewId;
	}
	public String getReviewName() {
		return reviewName;
	}
	public void setReviewName(String reviewName) {
		this.reviewName = reviewName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Boolean getVerified() {
		return verified;
	}
	public void setVerified(Boolean verified) {
		this.verified = verified;
	}
	

}
