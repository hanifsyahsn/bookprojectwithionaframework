package com.example.BookProjectHanif.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "rating")
public class Rating implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_rating_id_rating_seq")
	@SequenceGenerator(name = "generator_rating_id_rating_seq", sequenceName = "rating_id_rating_seq", schema = "public", allocationSize = 1)
	@Column(name = "rating_id", unique = true, nullable = false)
	private Long ratingId;
	@ManyToOne
	@JoinColumn(name = "book_id")
	private Book book;
	@ManyToOne
	@JoinColumn(name = "reviewer")
	private Reviewer reviewer;
	@Column(name = "rating_score", nullable = false)
	private int ratingScore;
	public Rating() {
		super();
	}
	public Rating(Long ratingId, Book book, Reviewer reviewer, int ratingScore) {
		super();
		this.ratingId = ratingId;
		this.book = book;
		this.reviewer = reviewer;
		this.ratingScore = ratingScore;
	}
	public Long getRatingId() {
		return ratingId;
	}
	public void setRatingId(Long ratingId) {
		this.ratingId = ratingId;
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public Reviewer getReviewer() {
		return reviewer;
	}
	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}
	public int getRatingScore() {
		return ratingScore;
	}
	public void setRatingScore(int ratingScore) {
		this.ratingScore = ratingScore;
	}
	
}
