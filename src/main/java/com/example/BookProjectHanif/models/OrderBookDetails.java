package com.example.BookProjectHanif.models;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
@Entity
@Table(name = "order_book_details")
public class OrderBookDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EmbeddedId
	private OrderBookDetailsKey orderKey;
	@Column(name = "quantity", nullable = false)
	private int quantity;
	@Column(name = "discount", nullable = false)
	private BigDecimal discount;
	@Column(name = "tax", nullable = false)
	private BigDecimal tax;
	@ManyToOne
	@MapsId("book_id")
	@JoinColumn(name = "book_id")
	private Book book;
	@ManyToOne
	@MapsId("order_id")
	@JoinColumn(name = "order_id")
	private Order order;
	public OrderBookDetails() {
		super();
	}
	
	public OrderBookDetails(OrderBookDetailsKey orderKey, int quantity, BigDecimal discount, BigDecimal tax, Book book,
			Order order) {
		super();
		this.orderKey = orderKey;
		this.quantity = quantity;
		this.discount = discount;
		this.tax = tax;
		this.book = book;
		this.order = order;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public OrderBookDetailsKey getOrderKey() {
		return orderKey;
	}
	public void setOrderKey(OrderBookDetailsKey orderKey) {
		this.orderKey = orderKey;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public BigDecimal getTax() {
		return tax;
	}
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}
	

}
