package com.example.BookProjectHanif.repositories;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectHanif.models.Author;
import com.example.BookProjectHanif.models.Book;
import com.example.BookProjectHanif.models.Publisher;
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
	
	@Query( value = "SELECT * from Book", nativeQuery = true)
	List<Book> findAllBook();
	
	@Query(value = "SELECT * from Book WHERE book_id = ?1", nativeQuery = true)
	Book findBookById(Long id);
	
	@Modifying
	@Query(value = "insert into Book (title, release_date, author_id, publisher_id, price) "
			+ "values(:title, :releaseDate, :authors, :publishers, :price)", nativeQuery = true)
	void insertBook(@Param("title") String title, @Param("releaseDate") Date releaseDate, @Param("authors") Author authors, @Param("publishers") Publisher publishers, @Param("price") BigDecimal price);
	
	@Modifying
	@Query(value = "delete from Book u WHERE u.bookId = ?1", nativeQuery = true)
	void deleteBook(Long id);
	
	@Modifying
	@Query(value = "update Paper set quality_Name = ?, price = ? where paper_id = ?", 
	  nativeQuery = true)
	void updatePaper(String qualityName, BigDecimal price, Long paperId);
	
	@Modifying
	@Query(value = "update Book set title = ?, release_date = ?, author_id = ?, publisher_id = ?, price = ? where book_id = ?", nativeQuery = true)
	void updateBook(String title, Date releaseDate, Author authors, Publisher publishers, BigDecimal price, Long bookId);
}
