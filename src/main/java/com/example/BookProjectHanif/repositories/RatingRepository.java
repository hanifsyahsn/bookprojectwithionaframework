package com.example.BookProjectHanif.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectHanif.models.Book;
import com.example.BookProjectHanif.models.Rating;
import com.example.BookProjectHanif.models.Reviewer;
@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {

	@Query(value = "SELECT * FROM Rating", nativeQuery = true)
	List<Rating> findAllRating();
	
	@Query(value = "SELECT * FROM Rating WHERE rating_id = ?", nativeQuery = true)
	Rating findRatingById(Long id);
	
	@Modifying
	@Query(value = "insert into Rating (book_id, reviewer, rating_score) values (:book, :reviewer, :ratingScore)", nativeQuery = true)
	void insertRating(@Param("book") Book book, @Param("reviewer") Reviewer reviewer, @Param("ratingScore") int ratingScore);
	
	@Modifying
	@Query(value = "delete from Rating WHERE rating_id = ?", nativeQuery = true)
	void deleteRatingById2(Long id);
	
	@Modifying
	@Query(value = "update Rating set book_id = ?, reviewer = ?, rating_score = ? where rating_id = ?", 
	  nativeQuery = true)
	void updateRating(Book book, Reviewer reviewer, int ratingScore, Long id);
}
