package com.example.BookProjectHanif.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectHanif.models.Author;
@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

	@Query(value = "SELECT * FROM Author", nativeQuery = true)
	List<Author> findAllAuthor();
	
	@Query(value = "SELECT * FROM Author WHERE author_id = ?", nativeQuery = true)
	Author findAuthorById(Long id);
	
	@Modifying
	@Query(value = "insert into Author (first_name, gender, last_name, age, contry, rating) values (:firstName, :gender, :lastName, :age, :country, :rating)", nativeQuery = true)
	void insertAuthor(@Param("firstName") String firstName, @Param("gender") String gender, @Param("lastName") String lastName, @Param("age") int age, @Param("country") String country, @Param("rating") String rating);
	
	@Modifying
	@Query(value = "delete from Author WHERE author_id = ?", nativeQuery = true)
	void deleteAuthorById2(Long id);
	
	@Modifying
	@Query(value = "update Author set first_name = ?, gender = ?, last_name = ?, age = ?, contry = ?, rating = ? where author_id = ?", 
	  nativeQuery = true)
	void updateAuthor(String firstName, String gender, String lastName, int age, String country, String rating, Long id);

}
