package com.example.BookProjectHanif.repositories;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectHanif.models.Customers;
import com.example.BookProjectHanif.models.Order;
@Repository
public interface OrderRepository extends JpaRepository<Order, Long>{

	@Query(value = "SELECT * FROM Orders", nativeQuery = true)
	List<Order> findAllOrder();
	
	@Query(value = "SELECT * FROM Orders WHERE order_id = ?", nativeQuery = true)
	Order findOrderById(Long id);
	
	@Modifying
	@Query(value = "insert into Orders (customers_id, order_date, total_order) values (:customers, :orderDate, :totalOrder)", nativeQuery = true)
	void insertOrder(@Param("customers") Customers customers, @Param("orderDate") Date orderDate, @Param("totalOrder") BigDecimal totalOrder);
	
	@Modifying
	@Query(value = "delete from Orders WHERE order_id = ?", nativeQuery = true)
	void deleteOrderById2(Long id);
	
	@Modifying
	@Query(value = "update Orders set customers_id = ?, order_date = ?, total_order = ? where order_id = ?", 
	  nativeQuery = true)
	void updateOrder(Customers customers, Date orderDate, BigDecimal totalOrder, Long id);
}
