package com.example.BookProjectHanif.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectHanif.models.Paper;
import com.example.BookProjectHanif.models.Publisher;
@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long> {

	@Query(value = "SELECT * FROM Publisher", nativeQuery = true)
	List<Publisher> findAllPub();
	
	@Query(value = "SELECT * FROM Publisher WHERE publisher_id = ?1",nativeQuery = true)
	Publisher findPubById(Long id);
	
	@Modifying
	@Query(value = "insert into Publisher (company_name, country, paper_id) values (:companyName, :country, :paper)", nativeQuery = true)
	void insertPub2(@Param("companyName") String companyName, @Param("country") String country, @Param("paper") Paper paper);
	
	@Modifying
	@Transactional
	@Query(value = "delete from Publisher u WHERE u.publisherId = ?1")
	void deletePubById(Long id);
	
	
	@Modifying
	@Query(value = "update Publisher set company_name = ?, country = ?, paper_id = ? where publisher_id = ?", 
	  nativeQuery = true)
	int updatePub(String companyName, String country, Long paperId, Long publisherId);

}
