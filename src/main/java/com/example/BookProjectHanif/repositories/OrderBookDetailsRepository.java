package com.example.BookProjectHanif.repositories;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.BookProjectHanif.models.OrderBookDetails;
import com.example.BookProjectHanif.models.OrderBookDetailsKey;
@Repository
public interface OrderBookDetailsRepository extends JpaRepository<OrderBookDetails, OrderBookDetailsKey> {

	@Query(value = "select * from order_book_details", nativeQuery = true)
	List<OrderBookDetails> findAllData();
	
	@Query(value = "select * from order_book_details WHERE book_id = ?1 and order_id = ?2", nativeQuery = true)
	OrderBookDetails findDataById(Long id1, Long id2);
	
//	@Modifying
//	@Query(value = "insert into order_book_details(quantity, discount, tax, book_id, order_id) "
//			+ "values (:quantity, :discount, :tax, :book, :order)", nativeQuery = true)
//	void insertData(@Param("quantity")int quantity, @Param("discount")BigDecimal discount, 
//			@Param("tax")BigDecimal tax, @Param("book")Long book, @Param("order")Long order);
	
	@Modifying
	@Transactional
	@Query(value = "delete from order_book_details  WHERE book_id = ?1 and order_id = ?2", nativeQuery = true)
	void deleteDataById(Long id, Long id2);
	
	@Modifying
	@Query(value = "update order_book_details set quantity = ?, discount = ?, tax = ?, book_id = ?, order_id = ? "
			+ "where book_id = ? and order_id = ?", 
	  nativeQuery = true)
	int updateData(int quantity, BigDecimal discount, BigDecimal tax, Long book, Long order, Long bookId, Long orderId);

}
