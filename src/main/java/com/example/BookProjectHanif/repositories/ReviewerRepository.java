package com.example.BookProjectHanif.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectHanif.models.Reviewer;
@Repository
public interface ReviewerRepository extends JpaRepository<Reviewer, Long>{

	@Query(value = "SELECT * FROM Reviewer", nativeQuery = true)
	List<Reviewer> findAllReviewer();
	
	@Query(value = "SELECT * FROM Reviewer WHERE review_id = ?", nativeQuery = true)
	Reviewer findReviewerById(Long id);
	
	@Modifying
	@Query(value = "insert into Reviewer (review_name, country, verified) values (:reviewName, :country, :verified)", nativeQuery = true)
	void insertReviewer(@Param("reviewName") String reviewName, @Param("country") String country, @Param("verified") Boolean verified);
	
	@Modifying
	@Query(value = "delete from Reviewer WHERE review_id = ?", nativeQuery = true)
	void deleteReviewerById2(Long id);
	
	@Modifying
	@Query(value = "update Reviewer set review_name = ?, country = ?, verified = ? where review_id = ?", 
	  nativeQuery = true)
	void updateReviewer(String customerName, String country, boolean verified, Long id);
}
