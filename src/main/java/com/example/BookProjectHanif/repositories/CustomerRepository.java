package com.example.BookProjectHanif.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectHanif.models.Customers;
@Repository
public interface CustomerRepository extends JpaRepository<Customers, Long>{
	
	@Query(value = "SELECT * FROM Customers", nativeQuery = true)
	List<Customers> findAllCustomers();
	
	@Query(value = "SELECT * FROM Customers WHERE customer_id = ?", nativeQuery = true)
	Customers findCustomerById(Long id);
	
	@Modifying
	@Query(value = "insert into Customers (customer_name, country, address, phone_number, postal_code, email) values (:customerName, :country, :address, :phone_number, :postalCode, :email)", nativeQuery = true)
	void insertCustomer(@Param("customerName") String customerName, @Param("address") String address, @Param("phone_number") String phoneNumber, @Param("postalCode") String postalCode, @Param("email") String email, @Param("country") String country);
	
	@Modifying
	@Query(value = "delete from Customers WHERE customer_id = ?", nativeQuery = true)
	void deleteCustomerById2(Long id);
	
	@Modifying
	@Query(value = "update Customers set customer_name = ?, country = ?, address = ?, phone_number = ?, postal_code = ?, email = ? where customer_id = ?", 
	  nativeQuery = true)
	void updateCustomer(String customerName, String country, String address, String phoneNumber, String postalCode, String email, Long id);

}
