package com.example.BookProjectHanif.repositories;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectHanif.models.Paper;


@Repository
public interface PaperRepository extends JpaRepository<Paper, Long>{
	
	@Query(value = "SELECT * FROM Paper", nativeQuery = true)
	List<Paper> findAllPaper();
	
	@Query(value = "SELECT * FROM Paper WHERE paper_id = ?1", nativeQuery = true)
	Paper findPaperById(Long id);
	
	@Modifying
	@Query(value = "insert into Paper (quality_name, price) values (:qualityName, :paperPrice)", nativeQuery = true)
	void insertPaper(@Param("qualityName") String qualityName, @Param("paperPrice") BigDecimal paperPrice);
	
	@Modifying
	@Query(value = "delete from Paper u WHERE u.paperId = ?1")
	void deletePaperById2(Long id);
	
	@Modifying
	@Query(value = "update Paper set quality_Name = ?, price = ? where paper_id = ?", 
	  nativeQuery = true)
	void updatePaper(String qualityName, BigDecimal price, Long paperId);


}
