package com.example.BookProjectHanif;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookProjectNewHanifApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookProjectNewHanifApplication.class, args);
	}

}
